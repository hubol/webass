export const Page = {
    get canvasElement()
    {
        return document.getElementById("canvas") as HTMLCanvasElement;
    },
    get fullscreenButtonElement()
    {
        return document.getElementById("fullscreenButton") as HTMLButtonElement;
    }
};