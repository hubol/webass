import {Page} from "./page";
import {runGameLoop} from "./core/gameLoop";
import {advanceKeyListener, Key, startKeyListener} from "./core/key";
import {advanceMouseListener, Mouse, startMouseListener} from "./core/mouse";

function useGlContext(glContext: WebGLRenderingContext)
{
    glContext.clearColor(0.0, 0.0, 1.0, 1.0);
    glContext.clear(glContext.COLOR_BUFFER_BIT);

    runGameLoop({ workSupplier: () => {
            advanceKeyListener();
            advanceMouseListener();

            glContext.clearColor(1, 1, 0, 1.0);
            if (Key.isDown("Space"))
            {
                glContext.clearColor(Math.random(), Math.random(), Math.random(), 1.0);
            }

            glContext.clear(glContext.COLOR_BUFFER_BIT);

            glContext.enable(glContext.SCISSOR_TEST);

            let size = 32;
            if (Key.justWentDown("ArrowUp") || Key.justWentUp("ArrowUp"))
                size = 48;

            glContext.scissor(Mouse.unitX * 640, (1 - Mouse.unitY) * 480, size, size);
            glContext.clearColor(0, 0, 0, 1.0);
            glContext.clear(glContext.COLOR_BUFFER_BIT);
            glContext.disable(glContext.SCISSOR_TEST);

            return Promise.resolve();
        }, targetFps: 30 });
}

function onWindowLoaded()
{
    Page.fullscreenButtonElement.onclick = () => Page.canvasElement.requestFullscreen();
    const glContext = Page.canvasElement.getContext("webgl");

    if (glContext === null)
    {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }

    startKeyListener();
    startMouseListener();
    useGlContext(glContext);
}

window.onload = onWindowLoaded;