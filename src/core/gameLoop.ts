import {sleep, sleepUntilSync} from "../util/sleep";

interface GameLoopArgs
{
    workSupplier: () => Promise<void>;
    targetFps: number;
}

export async function runGameLoop(args: GameLoopArgs)
{
    let baseTime = window.performance.now();
    let currentIteration = 0;

    while (true)
    {
        await args.workSupplier();
        currentIteration++;

        const currentTime = window.performance.now();
        const targetTime = baseTime + (currentIteration * 1000) / args.targetFps;

        if (targetTime > currentTime)
        {
            const remainingTime = targetTime - currentTime;
            if (remainingTime > 4)
                await sleep(remainingTime * 0.75);
            sleepUntilSync(targetTime);
        }
        else
        {
            baseTime = currentTime;
            currentIteration = 0;
        }
    }
}