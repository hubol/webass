import {Page} from "../page";
import {vec2} from "../math/vector";

export const Mouse = {
    get unitVec(): vec2
    {
        return { x: currentMouseState.unitX, y: currentMouseState.unitY };
    },
    get unitX()
    {
        return currentMouseState.unitX;
    },
    get unitY()
    {
        return currentMouseState.unitY;
    }
};

type ButtonCode = "Left" | "Middle" | "Right";

interface MouseState
{
    unitX: number;
    unitY: number;
    leftButtonDown: boolean;
    middleButtonDown: boolean;
    rightButtonDown: boolean;
}

interface WorkingMouseState
{
    clientX: number;
    clientY: number;
    leftButtonDown: boolean;
    middleButtonDown: boolean;
    rightButtonDown: boolean;
}

function createInitialMouseState(): MouseState
{
    return {
        leftButtonDown: false,
        middleButtonDown: false,
        rightButtonDown: false,
        unitX: 0,
        unitY: 0
    };
}

let previousMouseState: MouseState = createInitialMouseState();
let currentMouseState: MouseState = createInitialMouseState();
let workingMouseState: WorkingMouseState = {
    clientX: 0,
    clientY: 0,
    leftButtonDown: false,
    middleButtonDown: false,
    rightButtonDown: false
};

function handleKeyDown(event: MouseEvent)
{

}

function handleKeyUp(event: MouseEvent)
{

}

function handleMouseMove(event: MouseEvent)
{
    workingMouseState.clientX = event.clientX;
    workingMouseState.clientY = event.clientY;
}

let startedMouseListener = false;

export function startMouseListener()
{
    if (startedMouseListener)
        throw new Error("Cannot start mouse listener twice!");

    document.addEventListener("mousedown", handleKeyDown);
    document.addEventListener("mouseup", handleKeyUp);
    document.addEventListener("mousemove", handleMouseMove);

    startedMouseListener = true;
}

export function advanceMouseListener()
{
    if (!startedMouseListener)
        throw new Error("Mouse listener must be started to advance!");

    const boundingClientRect = Page.canvasElement.getBoundingClientRect();
    console.log(boundingClientRect);

    previousMouseState = currentMouseState;
    currentMouseState = {
        unitX: (workingMouseState.clientX - boundingClientRect.x) / boundingClientRect.width,
        unitY: (workingMouseState.clientY - boundingClientRect.y) / boundingClientRect.height,
        leftButtonDown: workingMouseState.leftButtonDown,
        rightButtonDown: workingMouseState.rightButtonDown,
        middleButtonDown: workingMouseState.middleButtonDown
    };
}