export interface vec2
{
    x: number;
    y: number;
}

export interface vec3
{
    x: number;
    y: number;
    z: number;
}