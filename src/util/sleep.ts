export function sleep(ms: number)
{
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function sleepUntilSync(targetTimeMilliseconds: number)
{
    while (window.performance.now() < targetTimeMilliseconds)
    {
        // nop
    }
}